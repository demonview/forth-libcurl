VFXLIBS=constants.fs prelude/vfx.fs libcurl.fs
iVFXLIBS=$(foreach x,$(VFXLIBS),include $x)
IFLIBS=constants.fs prelude/iforth.fs libcurl.fs
iIFLIBS=$(foreach x,$(IFLIBS),include $x)

constants.fs: grovel
	./grovel > $@

grovel: grovel.c
	$$(curl-config --cc) $$(curl-config --cflags) $$(curl-config --libs) -o $@ $<

clean::
	rm -fv wwwcat.vfx
reallyclean:: clean
	rm -fv grovel constants.fs

wwwcat.vfx: examples/wwwcat.vfx.fs $(VFXLIBS)
	vfxlin $(iVFXLIBS) include $< 'assign go to-do EntryPoint save $@ bye'

iforth.tests:: tests.fs
	iforth $(iIFLIBS) verbose needs -ttester include $< bye
