needs -dynlink

s" libcurl.so" library-open throw constant libcurl
s" curl_easy_init" libcurl library-find throw constant c-curl_easy_init 
s" curl_easy_setopt" libcurl library-find throw constant c-curl_easy_setopt
s" curl_easy_perform" libcurl library-find throw constant c-curl_easy_perform
s" curl_easy_cleanup" libcurl library-find throw constant c-curl_easy_cleanup

s" curl_slist_append" libcurl library-find throw constant c-curl_slist_append
s" curl_slist_free_all" libcurl library-find throw constant c-curl_slist_free_all
s" curl_easy_escape" libcurl library-find throw constant c-curl_easy_escape
s" curl_free" libcurl library-find throw constant c-curl_free

: curl_easy_init ( -- handle )  0 c-curl_easy_init foreign ;
: curl_easy_setopt ( handle option param -- curlcode )  3 c-curl_easy_setopt foreign ;
: curl_easy_perform ( handle -- curlcode )  1 c-curl_easy_perform foreign ;
: curl_easy_cleanup ( handle -- )  1 c-curl_easy_cleanup foreign drop ;

: curl_slist_append ( 0|slist zstr -- slist )  2 c-curl_slist_append foreign ;
: curl_slist_free_all ( slist -- ) 1 c-curl_slist_free_all foreign drop ;
: curl_easy_escape ( handle c-addr u -- a )  3 c-curl_easy_escape foreign ;
: curl_free ( z -- ) 1 c-curl_free foreign drop ;

[UNDEFINED] BUFFER: [IF] : BUFFER: ( size "name" -- ) CREATE ALLOT ; [THEN]

\ CURL_ERROR_SIZE is actually 256, with one byte reserved for NUL
CURL_ERROR_SIZE 1+ buffer: (curl-errors)
(curl-errors) 1+ constant curl-errors

: curlthrow ( 0|error -- ) \ throws on error
  if curl-errors zcount (curl-errors) c! 1- $throw then ;


\ x in the following is the CURLOPT_(READ|WRITE|SEEK)DATA value
\ 1. in the curl-readwriter case it should be an XT
\ 2. in the curl-file-* cases it should be a file
\ 3. in the curl-buffer-reader case it should an address to a ( c-addr u )
\ pair with u at the address and c-addr at the address + one cell, as by 2!

( c-addr size #members x -- n-handled )
' execute 4 CALLBACK curl-readwriter
:noname -rot * dup >r swap write-file drop r> ; 4 CALLBACK curl-file-writer
:noname -rot * swap read-file drop ; 4 CALLBACK curl-file-reader
:noname LOCAL a
  * a @ min tuck a cell+ @ -rot
  dup negate a +!  dup a cell+ +!  move ; 4 CALLBACK curl-buffer-reader
