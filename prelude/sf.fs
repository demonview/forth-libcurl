library libcurl.so
function: curl_easy_init ( -- handle )
function: curl_easy_setopt ( handle option param -- curlcode )
function: curl_easy_perform ( handle -- curlcode )
function: curl_easy_cleanup ( handle -- )

function: curl_slist_append ( 0|slist zstr -- slist )
function: curl_slist_free_all ( slist -- )
function: curl_easy_escape ( handle c-addr u -- z )
function: curl_free ( z -- )

\ CURL_ERROR_SIZE is actually 256, with one byte reserved for NUL
CURL_ERROR_SIZE buffer: curl-errors

: curlthrow ( 0|error -- ) \ throws on error
  if curl-errors zcount ERRMSG place -2 throw then ;

\ x in the following is the CURLOPT_(READ|WRITE|SEEK)DATA value
\ 1. in the curl-readwriter case it should be an XT
\ 2. in the curl-file-* cases it should be a file
\ 3. in the curl-buffer-reader case it should an address to a ( c-addr u )
\ pair with u at the address and c-addr at the address + one cell, as by 2!

( c-addr size #members x -- n-handled )
' execute 4 CB: curl-readwriter
:noname -rot * dup >r swap write-file drop r> ; 4 CB: curl-file-writer
:noname -rot * swap read-file drop ; 4 CB: curl-file-reader
:noname locals| a |
  * a @ min tuck a cell+ @ -rot
  dup negate a +!  dup a cell+ +!  move ; 4 CB: curl-buffer-reader
