variable (curl)
variable curl-headers
: curl ( -- handle )
  (curl) @ dup if exit then drop
  curl_easy_init dup 0= curlthrow dup (curl) !
  dup CURLOPT_ERRORBUFFER curl-errors curl_easy_setopt curlthrow
  curl-headers @ ?dup if
    over CURLOPT_HTTPHEADER rot curl_easy_setopt curlthrow
  then ;

: cleanup ( -- )
  curl curl_easy_cleanup
  0 (curl) !
  curl-headers @ ?dup if
    curl_slist_free_all
    0 curl-headers !
  then ;

: no-ssl-security ( -- )
  curl CURLOPT_SSL_VERIFYHOST   0 curl_easy_setopt curlthrow
  curl CURLOPT_SSL_VERIFYPEER   0 curl_easy_setopt curlthrow
  curl CURLOPT_SSL_VERIFYSTATUS 0 curl_easy_setopt curlthrow ;

: verbose ( -- ) curl CURLOPT_VERBOSE 1 curl_easy_setopt curlthrow ;

: curl-action ( z-url curlopt -- )
  curl swap 1 curl_easy_setopt curlthrow 
  curl CURLOPT_URL rot curl_easy_setopt curlthrow
  curl curl_easy_perform curlthrow ;

: get ( z-url -- ) CURLOPT_HTTPGET curl-action ;
: put ( z-url -- ) CURLOPT_PUT curl-action ;
: post ( z-url -- ) CURLOPT_POST curl-action ;

: writer! ( xt -- )
  curl CURLOPT_WRITEFUNCTION curl-readwriter curl_easy_setopt curlthrow
  curl CURLOPT_WRITEDATA rot curl_easy_setopt curlthrow ;

\ can't use libcurl's write-to-a-file default because it uses FILE*
: file-writer! ( file -- )
  curl CURLOPT_WRITEFUNCTION curl-file-writer curl_easy_setopt curlthrow
  curl CURLOPT_WRITEDATA rot curl_easy_setopt curlthrow ;

: postdata! ( z -- ) 
  curl CURLOPT_POSTFIELDS rot curl_easy_setopt curlthrow ;

2variable curl-buffer
variable /curl-buffer

: init-curl-buffer ( n -- )
  curl-buffer cell+ @ if drop  0 curl-buffer ! exit then
  dup allocate throw  swap /curl-buffer !  0 curl-buffer 2! ;
: >curl-buffer ( c-addr u -- )
  dup curl-buffer @ + /curl-buffer @ 2dup > if
    begin 2* 2dup < until nip dup /curl-buffer !
    curl-buffer cell+ @  swap resize throw  curl-buffer cell+ !
  else 2drop then tuck  curl-buffer 2@ + swap move  curl-buffer +! ;

: (output-to-string) ( a u #members -- u )  * tuck >curl-buffer ;
: output-to-string ( nk -- )
  1024 * init-curl-buffer
  ['] (output-to-string) writer! ;

0 constant headers
: end-headers ( 0 z"header1" ... z"headerN" -- )
  curl-headers @ begin over while
    swap curl_slist_append
  repeat nip dup curl-headers !
  curl CURLOPT_HTTPHEADER rot curl_easy_setopt curlthrow ;
