TESTING simple usage
: example ( -- z ) s\" https://example.com/\x00" drop ;
T{ example 2 output-to-string get
   curl-buffer cell+ @ s" <!doctype html>" tuck compare -> 0 }T

TESTING easy escape
T{ curl s" a + b" curl_easy_escape zcount s" a%20%2B%20b" compare -> 0 }T
