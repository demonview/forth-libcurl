include ../../constants.fs
include ../../prelude/iforth.fs
include ../../libcurl.fs
include ../refresher.fs

target 1 url localhost/1
target 2 url localhost/2
target 3 url localhost/3
target index url localhost
refresher

\ this constantly downloads the URLs, writing them to files in the current
\ directory, and then renaming them to the parent directory.

\ why the rename? so that anything looking at the files in the parent directory
\ will never see an incomplete download, as they would if the files were
\ downloaded directly to their destination.
