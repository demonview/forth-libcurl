variable targets \ list of ( next d-target z-url )
  \ next : address of the next target, or 0
  \ d-target : ( c-addr u ) pair taken as a filename
  \ z-url : ( z-addr ) zero-terminated string used as a URL

: target ( "destfile" -- )
  here parse-name tuck here swap dup allot move
  align here targets @ , targets !
  , , ;
: url ( "url" -- ) \ finish what TARGET started
  here cell+ , parse-name here swap dup allot move 0 c, ;

create ../path char . c, char . c, char / c, PATH_MAX allot
../path 3 + constant ../file

: ../rename ( c-addr u -- )
  2dup ../file swap move ../path over 3 + rename-file throw ;

: refresh ( z-url d-target -- )
  2dup w/o create-file throw dup file-writer! 3 roll get -rot ../rename
  close-file throw ;

: refresh-all ( -- )
  targets @ begin dup while
    dup 3 cells + @
    over cell+ 2@ refresh
  @ repeat drop ;

: refresher ( -- )
  begin refresh-all again ;
