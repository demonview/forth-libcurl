#! /usr/bin/env perl
use LWP::UserAgent;
use Time::HiRes qw(gettimeofday tv_interval);
use common::sense;
$! = 1;

bench1();
bench2();

sub bench1 {
  my $start = [gettimeofday];
  print "bench1 (simple): ";
  for (1..100) {
    my $ua = LWP::UserAgent->new;
    my $req = $ua->get('http://localhost/1');
    print STDERR $req->content;
  }
  printf "%.2fs\n", tv_interval($start, [gettimeofday]);
}

sub bench2 {
  my $start = [gettimeofday];
  print "bench2 (reuse \$ua): ";
  my $ua = LWP::UserAgent->new;
  for (1..100) {
    my $req = $ua->get('http://localhost/1');
    print STDERR $req->content;
  }
  printf "%.2fs\n", tv_interval($start, [gettimeofday]);
}
