include ../constants.fs
include ../prelude/iforth.fs
include ../libcurl.fs

: bench1 ( -- )
  cr ." bench1 (simple): " timer-reset
  100 0 do z" localhost/1" drop 2 file-writer! get cleanup loop
  .elapsed ;

: bench2 ( -- )
  cr ." bench2 (reuse curl): " timer-reset
  z" localhost/1" drop 2 file-writer! get
  curl 99 0 do dup curl_easy_perform curlthrow loop
  drop .elapsed ;

bench1
bench2
bye
