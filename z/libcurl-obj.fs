class curl-version-info-data
  variable age

  \ present when age >=0
  variable 'version
  variable #version
  variable 'host
  variable features
  2 cells buffer: 'ssl-version  \ second cell unused
  variable 'libz-version
  variable protocols
  
  \ present when age >=1
  variable 'ares
  variable #ares

  \ present when age >=2
  variable 'libidn

  \ present when age >=3
  variable #iconv-version

  variable 'libssh-version

  : show ( -- )
    cr ." version info age: " age ?
    cr ." version: " 'version @ zcount type 
    cr ." host: " 'host @ zcount type
    cr ." ssl-version: " 'ssl-version @ zcount type
    cr ." libz-version: " 'libz-version @ zcount type ;
end-class

3 curl_version_info constant curl-version

: .version ( -- )  curl-version USING curl-version-info-data show ;

\ cb ( a size #members a-data -- size )
: get-with ( z-url cb -- curlcode )
  curl_easy_init dup >r 0= abort" failed to init curl"
  r@ CURLOPT_VERBOSE 1 curl_easy_setopt abort" failed to set verbose"
  r@ CURLOPT_WRITEFUNCTION rot curl_easy_setopt abort" failed to set write callback"
  r@ CURLOPT_URL rot curl_easy_setopt abort" failed to set curl URL"
  r@ curl_easy_perform
  r> curl_easy_cleanup ;

: (dumper) ( a size #members a-data -- size )
  drop * tuck type ;
' (dumper) 4 CALLBACK curl-dumper

: ZEN ( c-addr u -- z-addr )
  here -rot here over + 0 swap c! here swap move ;

: get-type ( "url" -- curlcode )
  10 parse ZEN curl-dumper get-with ;
